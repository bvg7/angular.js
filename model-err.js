let AlertErrorsDict = {
    400: {type: "danger", name: "Ошибка", value: "400 Bad Request"},
    401: {type: "danger", name: "Ошибка", value: "401 Unauthorized"},
    403: {type: "danger", name: "Ошибка", value: "403 Forbidden. У Вас нет доступа к этой странице!"},
    404: {type: "danger", name: "Ошибка", value: "404 Not found. Запрашиваемый ресурс не найден!"},
    500: {type: "danger", name: "Ошибка", value: "500 Internal Error"},
    unknown: {type: "danger", name: "Ошибка", value: "unknown"}
};

app.factory("AlertErrors", () => {
    let err = AlertErrorsDict;
    function get(error) {
        if (!error || error.status === 0) return undefined;
        let error_dict = err[error.status]
        if (!error_dict) {
            error_dict = err.unknown;
            error_dict.value = error;
        }

        return error_dict;
    };

    return {
        get: get,
    };
});

app.factory('CurrentObject', function () {
    var persistObject = {};
    function set(objectName, data) {
        persistObject[objectName] = data;
    };
    function get(objectName) {
        return persistObject[objectName];
    };

    return {
        set: set,
        get: get,
        currentObject: persistObject,
    }
});
