function isQuoted(str/*: string*/) {
    if (!str) return false;
    let s = str.trim();
    // console.log("isQuoted s first, last:", s.slice(0, 1), s.slice(-1));
    if (["'", '"'].includes(s.slice(0, 1)) && ["'", '"'].includes(s.slice(-1))) 
        return true;
    return false;

}

function delQuotes(str) {
    if (!str) return str;
    let s = str.trim();
    if (isQuoted(s)) {
        s = s.slice(1, -1);
        // console.log("delQuotes s:", s);
        return s;
    }
    return s;
}

function isEmpty(obj) {
    return Object.keys(obj).length == 0 ? true : false;
}

function str2bool(value/*: String*/) {
    if (value == null || value == undefined) return null;
    switch (typeof value) {
        case "bigint":
        case "number":
            if (value > 0) return true;
            return false;
        case "boolean": return value;
        case "symbol":
        case "string":
            s = value.toLowerCase();
            if (["1", "t", "true", "y", "yes", "on", "да"].includes(s)) return true;
            if (["0", "f", "false", "n", "no", "off", "нет"].includes(s)) return false;
            return null;
        default:            
            break;
    }
    return null;
}